# README #

wxPython basd BMX random gate demo
Some details of sequence from
https://github.com/dfrencham/rad-gate/blob/master/doc/BMXCadence.md

Initial code from here.
https://www.raspberrypi.org/forums/viewtopic.php?f=37&t=124080
I don't have any hardware... so this code was modified to provide demo on a PC
LED_Pin, Buzzer, and Button classes created to simulate operation without the gpiozero hardware

Tested on Linux Ubuntu 16.04 64bit - sometimes problem [xcb] Unknown request in queue. Failure at xcb_io.c:179
Tested OK on Win7 32 bit
