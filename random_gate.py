#!/usr/bin/python

import Queue
import  wx
from wx.lib.pubsub import setupkwargs
from wx.lib.pubsub import pub

import wave
import pygame
import random
from array import array
import threading

#from gpiozero import LED
#from gpiozero import Button
#from gpiozero import Buzzer

"""
wxPython based BMX random gate demo. Using for testing ideas while building BMX start gate project.

Some references

https://github.com/dfrencham/rad-gate/blob/master/doc/BMXCadence.md
(for use with Arduino)
Am using my code below for inspiration to possibly rewrite rad-gate sequence.cpp to suit my needs.

Initial code is from here. Was possiblly going to use RasberryPi...
https://www.raspberrypi.org/forums/viewtopic.php?f=37&t=124080
I don't have any hardware... so this code was modified to provide demo on a PC (tested on Linux Ubuntu 16.04
LED_Pin, Buzzer, and Button classes created to simulate operation without the gpiozero hardware)

"""

__version__ = '0.2'

DELAY_INTERVAL_MS = 60
DELAY_DROP_TONE_MS = 60
GATE_ACTIVE_MS = 300
DELAY_DROP_TONE_FINAL_MS = 2250

### Below detail actions to happen at which times (after messages) (use monospace font to view)
"""
Action Plan

ItemTime(ms)Action1	    Action2	        Action3

0	  0	    Set LED1	Play Tone 632
1	 60		            Stop Tone
2	120	    Set LED2	Play Tone 632
3	180		            Stop Tone
4	240	    Set LED3	Play Tone 632
5	300		            Stop Tone
6	360	    Set LED4	Play Tone 632	Gate Activate
7	660	                                Gate Deactivate  # set time per solenoid requirements
8  2610	    Reset LEDS	Stop Tone
"""

### These are the required timings
T0 = 0
T1 = T0 + DELAY_DROP_TONE_MS
T2 = T1 + DELAY_INTERVAL_MS
T3 = T2 + DELAY_DROP_TONE_MS
T4 = T3 + DELAY_INTERVAL_MS
T5 = T4 + DELAY_DROP_TONE_MS
T6 = T5 + DELAY_INTERVAL_MS
T7 = T6 + GATE_ACTIVE_MS
T8 = T6 + DELAY_DROP_TONE_FINAL_MS

times = [T0, T1, T2, T3, T4, T5, T6, T7, T8]

### the actions refer to each line of seq list below
### and to the Action plan above
### format is int, bool, bool
### -1 means ignore

actions = ['set_led', 'play_tone', 'gate_act']

seq= [ \
[ 1,  1, -1],
[-1,  0, -1],
[ 2,  1, -1],
[-1,  0, -1],
[ 3,  1, -1],
[-1,  0, -1],
[ 4,  1,  1],
[-1, -1,  0],
[-1,  0, -1],
]

"""Set the corresponding pin numbers on raspberry pi for leds"""
leds = {'red' : 18 , 'yellow' : 23,  "yellow2" : 27,  'green' :24}

def change_intensity(color, fac):
    """Used by GuiLED to provide colour contours on GUI LEDs"""
    rgb = [color.Red(), color.Green(), color.Blue()]
    for i, intensity in enumerate(rgb):
        rgb[i] = min(int(round(intensity*fac, 0)), 255)

    return wx.Colour(*rgb)

class Note(pygame.mixer.Sound):
    """ Used to play tones. Set the volume below """
    volume = 0.1

    def __init__(self, frequency, volume=volume):
        self.frequency = frequency
        pygame.mixer.Sound.__init__(self, buffer=self.build_samples())
        self.set_volume(volume)

    def build_samples(self):
        period = int(round(pygame.mixer.get_init()[0] / self.frequency))
        samples = array("h", [0] * period)
        amplitude = 2 ** (abs(pygame.mixer.get_init()[1]) - 1) - 1
        for _time in xrange(period):
            if _time < period / 2:
                samples[_time] = amplitude
            else:
                samples[_time] = -amplitude
        return samples

class LED_Pin(object):
    """Class created as hardware substitute"""
    def __init__(self, pin, color):
        #print "Init %s LED pin %s" % ( color, pin)
        self.color = color

    def on(self):
        #print "%s LED is on" % self.color
        pass

    def off(self):
        #print "%s LED is off" % self.color
        pass

class Buzzer(object):
    """Class created as hardware substitute"""
    def __init__(self, pin):
        #print "Init Buzzer pin %s" % pin
        pygame.mixer.pre_init(16000, 16, 1, 1024)
        pygame.init()

    def on(self):
        #print "Buzzer is on"
        self.mysound = Note(632).play(-1)

    def off(self):
        #print "Buzzer is off"
        self.mysound.stop()

class Button(object):
    """Class created as hardware substitute"""
    def __init__(self, pin):
        #print "Init Button pin %s" % pin
        self.pin = pin

    def is_pressed(self):
        #print "Button %s is pressed" % self.pin
        return False

class Loop(threading.Thread):
    """Thread waits for button activation then runs the random sequence and gate drop """

    def __init__(self, notify_window, q):
        threading.Thread.__init__(self)
        self.q = q

        ### Set LED pins
        self.red_led = LED_Pin(leds['red'], 'red')
        self.yellow_led = LED_Pin(leds['yellow'], 'yellow')
        self.yellow2_led = LED_Pin(leds['yellow2'], 'yellow2')
        self.green_led = LED_Pin(leds['green'], 'green')
        self.relay = LED_Pin(4, None) # cheeky relay function

        ### Set Button pins
        self.button = Button(25)
        self.bp = self.button.is_pressed()

        ### Set Buzzer pins
        self.buzzer = Buzzer(22)

    def run(self):
        self.main()

    def main(self):
        """Activate full sequence initiated by the button push
        voice messages play
        random start delay - of between .1s and 2.7s
        gate drop sequence
        """

        while self.bp:
            if not self.q.empty():
                self.bp = not self.q.get_nowait()
            else:
                self.bp = False
            ### print "BP is %s" % self.bp
            
            pygame.time.delay(1000)  # 1.0 second
            self.playWav('1.wav')    # 1.5 seconds
            pygame.time.delay(1800)  # 1.8 seconds
            self.playWav('2.wav')    # 2.0 seconds
            _delay = int(random.uniform(100, 2700)) # random 0.1 - 2.7 seconds
            #print "Wait %s mseconds" % _delay
            pygame.time.delay(_delay)
            self.gateDrop()
            self.bp = False

        while not self.bp:
            ### print "BP Queue empty is %s" %  self.q.empty()
            if not self.q.empty():
                self.bp = self.q.get_nowait()
                ### print "BP is %s" % self.bp
                self.main()
            pygame.time.wait(500)

    def set_led(self, val):
        if val == 1:
            self.red_led.on()
            pub.sendMessage('led_event', message='',  arg2=[0, 5, 6, 7])
        elif val == 2:
            self.yellow_led.on()
            pub.sendMessage('led_event', message='',  arg2=[0, 1, 6, 7])
        elif val == 3:
            self.yellow2_led.on()
            pub.sendMessage('led_event', message='',  arg2=[0, 1, 2, 7])
        elif val == 4:
            self.green_led.on()
            pub.sendMessage('led_event', message='',  arg2=[0, 1, 2, 3])

    def play_tone(self, bool_val):
        if bool_val:
            self.buzzer.on()
        else:
            self.buzzer.off()

    def gate_act(self, bool_val):
        if bool_val:
            self.relay.on()
            self.playWav('clunk.wav') ### Simulating solenoid clunk
        else:
            self.relay.off()
            self.playWav('woosh.wav') ### Simulating solenoid unclunk
            
        

    def gateDrop(self):
        """ Run the Gate drop sequence
        Set LEDs, tones, gate drop - see Action plan at  the top
        pubsendMessage sends LED state info back to BMX_Random_Gate GUI.
        Sequence of events is shown at top of file
        """
        #pygame.init() # not required as already init
        
        def millis():
            return pygame.time.get_ticks()

        now = 0
        offset = millis() 

        for i, v in enumerate(seq):
            while times[i] > now:
                now = millis() - offset

            #print"# Item %s at time %s ms" % (i, now)

            for j, val in enumerate(v):
                if val >= 0:
                    getattr(self, actions[j])(val)

        ### reset gui leds - could make as 5 in seq list and add to set_led()
        pub.sendMessage('led_event', message='',  arg2=[4, 5, 6, 7])
        self.red_led.off()
        self.yellow_led.off()
        self.yellow2_led.off()
        self.green_led.off()


    def playWav(self, file_path):
        ''' plays wav files using pygame'''
        pygame.init()
        file_wav = wave.open(file_path)
        frequency = file_wav.getframerate()
        pygame.mixer.pre_init(frequency, 8, 1, 4096)
        pygame.mixer.init()

        pygame.init()
        pygame.mixer.music.load(file_path)
        pygame.mixer.music.play()
        #print "Playing %s" % file_path
        while pygame.mixer.music.get_busy() == True:
            continue
        #pygame.mixer.quit()


class BMX_Random_Gate(wx.Frame):
    """ Creates a wxPython GUI to show LED  start sequence, and starts sequence from button click or <spacebar>"""

    def __init__(self, title, pos):
        wx.Frame.__init__(self, None, wx.ID_ANY, 'BMX Random Gate', pos=(800, 150), )

        self.Bind(wx.EVT_CLOSE, self.on_exit)
        self.panel = wx.Panel(self)

        self.create_gui()

        pub.subscribe(self.LEDs_Set, 'led_event')
        self.q1 = Queue.Queue()
        self.q1.put(False)  ### False indicate to loop that button is not initailly pressed
        self.loop_thread = Loop(self, self.q1)
        self.loop_thread.daemon = True
        self.loop_thread.start()

    def onKeyPress(self, event):
        """ On key press event - if spacebar then start sequence"""
        keycode = event.GetKeyCode()
        if keycode == wx.WXK_SPACE:
            event.Skip()
            self.bp = True
            self.q1.put(self.bp)

    def onStartBtn(self, event):
        """ On start button click - start sequence"""
        event.Skip()
        self.bp = True
        self.q1.put(self.bp)

    def create_gui(self):
        """ Creating GUI - 4 sets of 'LEDs' and a start button"""
        self.GuiLED1 = Gui_LED(self.panel, 4) ### red
        self.GuiLED2 = Gui_LED(self.panel, 5) ### yellow
        self.GuiLED3 = Gui_LED(self.panel, 6) ### yellow2
        self.GuiLED4 = Gui_LED(self.panel, 7) ### green

        self.start_btn = wx.Button(self.panel, label='START')
        self.Bind(wx.EVT_KEY_DOWN, self.onKeyPress, self.start_btn)
        self.Bind(wx.EVT_BUTTON, self.onStartBtn, self.start_btn)
        self.start_btn.SetFocus()

        self.vbox = wx.BoxSizer(wx.VERTICAL)
        self.vbox.Add(self.GuiLED1, border=5, flag= wx.ALL | wx.ALIGN_CENTER_HORIZONTAL)
        self.vbox.Add(self.GuiLED2, border=5, flag= wx.ALL | wx.ALIGN_CENTER_HORIZONTAL)
        self.vbox.Add(self.GuiLED3, border=5, flag= wx.ALL | wx.ALIGN_CENTER_HORIZONTAL)
        self.vbox.Add(self.GuiLED4, border=5, flag= wx.ALL | wx.ALIGN_CENTER_HORIZONTAL)
        self.vbox.Add(self.start_btn, border=5, flag= wx.ALL | wx.ALIGN_CENTER_HORIZONTAL)

        self.panel.SetSizerAndFit(self.vbox)
        self.SetClientSize(self.vbox.GetSize())
        self.Show(True)

    def LEDs_Set(self, message, arg2):
        """Set LED colors according to sequence stage. Colors selection is from Gui_LED list of colors"""
        self.GuiLED1.SetState(arg2[0])
        self.GuiLED2.SetState(arg2[1])
        self.GuiLED3.SetState(arg2[2])
        self.GuiLED4.SetState(arg2[3])

    def on_exit(self, event):
        print "Exiting..."
        self.Destroy()

class Gui_LED(wx.Control):
    """ Based on code from http://code.activestate.com/recipes/533125-wxpython-led-control/
    Creates the virtual LEDs from ASCII data - hint: to see the data format better choose monospaced font"""

    ### Custom colors
    Red = wx.Colour(220, 10, 10)
    DarkRed = wx.Colour(150, 10, 10)
    Yellow = wx.Colour(250, 200, 0)
    DarkYellow = wx.Colour(114, 108, 13)
    Green= wx.Colour(10, 220, 10)
    DarkGreen = wx.Colour(0, 76, 2)

    def __init__(self, parent, col, id=-1,
        colors=[Red , Yellow,  Yellow,  Green,  DarkRed , DarkYellow,  DarkYellow,  DarkGreen],
        pos=(-1,-1), style=wx.NO_BORDER):

        size = (140, 50)
        wx.Control.__init__(self, parent, id, pos, size, style)
        self.MinSize = size
        self._colors = colors
        self._state = -1
        self.SetState(col)
        self.Bind(wx.EVT_PAINT, self.OnPaint, self)

    def SetState(self, i):
        if i < 0:
            raise ValueError, 'Cannot have a negative state value.'
        elif i >= len(self._colors):
            raise IndexError, 'There is no state with an index of %d.' % i
        elif i == self._state:
            return

        self._state = i
        base_color = self._colors[i]
        light_color = change_intensity(base_color, 1.15)
        shadow_color = change_intensity(base_color, 1.07)
        highlight_color = change_intensity(base_color, 1.25)

        ascii_led = '''
        000000-----000000000000-----000000000000-----000000000000-----000000000000-----000000000000-----000000000000-----000000000000-----000000
        0000---------00000000---------00000000---------00000000---------00000000---------00000000---------00000000---------00000000---------0000
        000-----------000000-----------000000-----------000000-----------000000-----------000000-----------000000-----------000000-----------000
        00-----XXX----=0000-----XXX----=0000-----XXX----=0000-----XXX----=0000-----XXX----=0000-----XXX----=0000-----XXX----=0000-----XXX----=00
        0----XX**XXX-===00----XX**XXX-===00----XX**XXX-===00----XX**XXX-===00----XX**XXX-===00----XX**XXX-===00----XX**XXX-===00----XX**XXX-===0
        0---X***XXXXX===00---X***XXXXX===00---X***XXXXX===00---X***XXXXX===00---X***XXXXX===00---X***XXXXX===00---X***XXXXX===00---X***XXXXX===0
        ----X**XXXXXX====----X**XXXXXX====----X**XXXXXX====----X**XXXXXX====----X**XXXXXX====----X**XXXXXX====----X**XXXXXX====----X**XXXXXX====
        ---X**XXXXXXXX===---X**XXXXXXXX===---X**XXXXXXXX===---X**XXXXXXXX===---X**XXXXXXXX===---X**XXXXXXXX===---X**XXXXXXXX===---X**XXXXXXXX===
        ---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===
        ---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===
        ----XXXXXXXXX====----XXXXXXXXX====----XXXXXXXXX====----XXXXXXXXX====----XXXXXXXXX====----XXXXXXXXX====----XXXXXXXXX====----XXXXXXXXX====
        0---XXXXXXXXX===00---XXXXXXXXX===00---XXXXXXXXX===00---XXXXXXXXX===00---XXXXXXXXX===00---XXXXXXXXX===00---XXXXXXXXX===00---XXXXXXXXX===0
        0---=XXXXXXX====00---=XXXXXXX====00---=XXXXXXX====00---=XXXXXXX====00---=XXXXXXX====00---=XXXXXXX====00---=XXXXXXX====00---=XXXXXXX====0
        00=====XXX=====0000=====XXX=====0000=====XXX=====0000=====XXX=====0000=====XXX=====0000=====XXX=====0000=====XXX=====0000=====XXX=====00
        000===========000000===========000000===========000000===========000000===========000000===========000000===========000000===========000
        0000=========00000000=========00000000=========00000000=========00000000=========00000000=========00000000=========00000000=========0000
        000000=====000000000000=====000000000000=====000000000000=====000000000000=====000000000000=====000000000000=====000000000000=====000000
        000000-----000000000000-----000000000000-----000000000000-----000000000000-----000000000000-----000000000000-----000000000000-----000000
        0000---------00000000---------00000000---------00000000---------00000000---------00000000---------00000000---------00000000---------0000
        000-----------000000-----------000000-----------000000-----------000000-----------000000-----------000000-----------000000-----------000
        00-----XXX----=0000-----XXX----=0000-----XXX----=0000-----XXX----=0000-----XXX----=0000-----XXX----=0000-----XXX----=0000-----XXX----=00
        0----XX**XXX-===00----XX**XXX-===00----XX**XXX-===00----XX**XXX-===00----XX**XXX-===00----XX**XXX-===00----XX**XXX-===00----XX**XXX-===0
        0---X***XXXXX===00---X***XXXXX===00---X***XXXXX===00---X***XXXXX===00---X***XXXXX===00---X***XXXXX===00---X***XXXXX===00---X***XXXXX===0
        ----X**XXXXXX====----X**XXXXXX====----X**XXXXXX====----X**XXXXXX====----X**XXXXXX====----X**XXXXXX====----X**XXXXXX====----X**XXXXXX====
        ---X**XXXXXXXX===---X**XXXXXXXX===---X**XXXXXXXX===---X**XXXXXXXX===---X**XXXXXXXX===---X**XXXXXXXX===---X**XXXXXXXX===---X**XXXXXXXX===
        ---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===
        ---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===---XXXXXXXXXXX===
        ----XXXXXXXXX====----XXXXXXXXX====----XXXXXXXXX====----XXXXXXXXX====----XXXXXXXXX====----XXXXXXXXX====----XXXXXXXXX====----XXXXXXXXX====
        0---XXXXXXXXX===00---XXXXXXXXX===00---XXXXXXXXX===00---XXXXXXXXX===00---XXXXXXXXX===00---XXXXXXXXX===00---XXXXXXXXX===00---XXXXXXXXX===0
        0---=XXXXXXX====00---=XXXXXXX====00---=XXXXXXX====00---=XXXXXXX====00---=XXXXXXX====00---=XXXXXXX====00---=XXXXXXX====00---=XXXXXXX====0
        00=====XXX=====0000=====XXX=====0000=====XXX=====0000=====XXX=====0000=====XXX=====0000=====XXX=====0000=====XXX=====0000=====XXX=====00
        000===========000000===========000000===========000000===========000000===========000000===========000000===========000000===========000
        0000=========00000000=========00000000=========00000000=========00000000=========00000000=========00000000=========00000000=========0000
        000000=====000000000000=====000000000000=====000000000000=====000000000000=====000000000000=====000000000000=====000000000000=====000000
        '''.strip()

        xpm = ['136 34 5 1', ### width height ncolors chars_per_pixel
               '0 c None',
               'X c %s' % base_color.GetAsString(wx.C2S_HTML_SYNTAX).encode('ascii'),
               '- c %s' % light_color.GetAsString(wx.C2S_HTML_SYNTAX).encode('ascii'),
               '= c %s' % shadow_color.GetAsString(wx.C2S_HTML_SYNTAX).encode('ascii'),
               '* c %s' % highlight_color.GetAsString(wx.C2S_HTML_SYNTAX).encode('ascii')]

        xpm += [s.strip() for s in ascii_led.splitlines()]

        self.bmp = wx.BitmapFromXPMData(xpm)
        self.Refresh()

    def GetState(self):
        return self._state

    State = property(GetState, SetState)

    def OnPaint(self, e):
        dc = wx.PaintDC(self)
        dc.DrawBitmap(self.bmp, 0, 0, True)


class BRG(wx.App):
    def OnInit(self):
        frame = BMX_Random_Gate("BMX Random Gate", (10, 10))
        frame.Show()
        self.SetTopWindow(frame)

        return True

if __name__ == '__main__':
    app = BRG(False)
    app.MainLoop()

